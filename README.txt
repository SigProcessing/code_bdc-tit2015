Version
--------
Companion Code Version: 1.0


License
--------
This companion Code for "Conditioning of Random Block Subdictionaries with Applications to Block-Sparse Recovery and Regression," (DOI:10.1109/TIT.2015.2429632) by W. Bajwa, M. Duarte, M. and R. Calderbank is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/


Citation
---------
Any part of this code used in your work should be cited as follows:

W.U. Bajwa, M.F. Duarte, and R. Calderbank, "Conditioning of random block subdictionaries with applications to block-sparse recovery and regression," IEEE Trans. Information Theory, 2015, Companion Code, ver. 1.0.


Reporting of issues
--------------------
Any issues in this code should be reported to W.U. Bajwa and M.F. Duarte. However, this companion code is being provided on an "As IS" basis to support the ideals of reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.


Computational environment
--------------------------
This code has been tested in the following computational environments. While it can run in other environments also, we can neither provide such guarantees nor can help you make it compatible in other environments.

* PC: Windows 7 with Matlab R2011b and Matlab R2014a
* Mac: Mac OS X 10.10.4 with Matlab 8.3.0.532 (R2014a)


Instructions
-------------

** Script1_generate_paper_figures.m
	--> The directory 'Data' contains the dataset used in the paper, which can be used to generate Figures 1, 2, and 3 in the paper using this script.


** Script2_test_fixed_coherence.m
	--> This script can be used to regenerate the dataset of Figure 1 in the paper. It uses the dataset of 'random_matrices_output.mat' in the directory 'Data'. This script relies on the SPGL1 toolbox, which has been included in this package as a courtesy under the GNU LGPL Version 2.1. Please read 'COPYING.doc' in the directory 'SPGL1' for detailed license information.

	--> SPGL1 must be initialized before this script can be used. Detailed instructions for SPGL1 initialization are available in 'README.doc' within the 'SPGL1' directory. We have also included a script 'SPGL1_init.m' in this pacakge that can possibly be used for SPGL1 initialization. **IMPORTANT:** If you run spgdemo.m then you must restart Matlab before running any of these scripts because of the use of legacy random number generators in spgdemo.m.


** Script3_test_minmax_coherence_recovery.m
	--> This script can be used to regenerate the dataset of Figure 2 in the paper. It uses the dataset of 'random_matrices_output.mat' in the directory 'Data'. This script relies on the SPGL1 toolbox, which has been included in this package as a courtesy under the GNU LGPL Version 2.1. Please read 'COPYING.doc' in the directory 'SPGL1' for detailed license information.

	--> SPGL1 must be initialized before this script can be used. Detailed instructions for SPGL1 initialization are available in 'README.doc' within the 'SPGL1' directory. We have also included a script 'SPGL1_init.m' in this pacakge that can possibly be used for SPGL1 initialization. **IMPORTANT:** If you run spgdemo.m then you must restart Matlab before running any of these scripts because of the use of legacy random number generators in spgdemo.m.


** Script4_test_minmax_coherence_regression.m
	--> This script can be used to regenerate the dataset of Figure 3 in the paper. It uses the dataset of 'random_matrices_output.mat' in the directory 'Data'. This script relies on the SpaRSA toolbox, part of which has been included in this package as a courtesy under the GNU GPL Version 2.0. Please read 'README.txt' in the directory 'SpaRSA' for detailed license information.


** generate_random_matrices.m
	--> This is a function (Usage: generate_random_matrices) that can be used to generate block-structured matrices with different spectral norms and coherences, as discussed in the paper (including the ones reported in Table I and Table II of the paper).