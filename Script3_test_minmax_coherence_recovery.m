% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Conditioning of Random Block Subdictionaries with Applications to Block-Sparse Recovery and Regression," (DOI:10.1109/TIT.2015.2429632) by W. 
% Bajwa, M. Duarte, M. and R. Calderbank is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% W.U. Bajwa, M.F. Duarte, and R. Calderbank, "Conditioning of random block subdictionaries with applications to block-sparse recovery and regression," IEEE Trans. 
% Information Theory, 2015, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and M.F. Duarte. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

addpath Data SPGL1

% Parameters
N = 5000; % Signal lenght
G = 10; % Group size
K = 20; % Number of groups
Ms = round((sqrt(2*log(N/G-K))+sqrt(G))^2)+K*G; % Number of measurements prescribed by [Rao, Recht, Nowak]
% Set number of spectral norm multipliers
NumTau = 10;
% Set number of random block sparse signal trials.
numiter = 1000;
% Set number of measurements
M = round(3.5*Ms);
% Set values of sparsity K to attempt
Kvec = 5:3:50;

SDRmat = zeros(NumTau,2,length(Kvec),numiter); % Matrix for signal recovery results
SupRecmat = zeros(NumTau,2,length(Kvec),numiter); %  Matrix for model selection results

% Load matrix ensemble
load random_matrices_output

% For multiple values of spectral norm multiplier
for tau = 1:NumTau,
    % Sort matrices by coherence value
    [~,sortidx] = sort(Acohall(tau,:),'ascend');
    % For extreme (min/max) values of coherence
    for cnum = 1:2,
        % Select matrix with extreme coherence
        switch cnum
            case 1,
                testidx = sortidx(1);
            case 2,
                testidx = sortidx(end);
        end
        % Build selected random matrix
        rng(testidx);
        A = randn(M,N);
        % Normalize columns
        ANorm = sqrt(diag(A'*A));
        An = A./(ones(M,1)*ANorm');
        % Calculate SVD to adjust spectral nrom
        [U,S,V] = svd(An);
        % Change spectral norm, rest of singular values
        bfact = sqrt((N-(tau*S(1))^2)/(N-S(1)^2));
        % Adjust singular value matrix
        S2 = [diag([S(1)*tau; bfact*diag(S(2:M,2:M))]) zeros(M,N-M)];
        % Build matrix from SVD
        Am = U*S2*V';
        % Normalize columns again
        AmNorm = sqrt(diag(Am'*Am));
        Amn = Am./(ones(M,1)*AmNorm');
        % Test matrix using group-sparse BP over numiter random signals per value of K
        for iter = 1:numiter,
            % For each value of K
            for Kidx = 1:length(Kvec),
                % Build random block sparse signal, normalize
                K = Kvec(Kidx);
                x = zeros(G,N/G);
                idx = randperm(N/G);
                x(:,idx(1:K)) = randn(G,K);
                x = x(:)/norm(x(:));
                % Compute measurements
                y = Amn*x;
                % Set up solver for group-sparse BP
                groups  = reshape(ones(G,1)*(1:(N/G)),N,1); % Definition of groups
                opts = spgSetParms('verbosity',0);          % Turn off the SPGL1 log output
                xr    = spg_group(Amn,y,groups,0,opts);     % Solve group-sparse BP
                % Retrieve group support of recovered signal
                idxa = find(sum(reshape(abs(xr),G,N/G),1)> 1e-3);
                % Record success or failure for support recovery, invertibility
                % of the submatrix of Amn involved
                SupRecmat(tau,cnum,Kidx,iter) = isempty(setxor(idxa,idx(1:K))) & (rank(Amn(:,find(abs(x)>1e-3)'))==length(find(abs(x)>1e-3)));
                % Calculate and store block BP recovery signal-to-distortion ratio
                sdr = 20*log10(norm(x)/norm(x-xr));
                SDRmat(tau,cnum,Kidx,iter) = sdr;
            end
        end
    end
    save test_minmax_coherence_recovery_output Kvec SDRmat SupRecmat
end