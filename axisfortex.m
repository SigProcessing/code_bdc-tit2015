% Helper script
% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Conditioning of Random Block Subdictionaries with Applications to Block-Sparse Recovery and Regression," (DOI:10.1109/TIT.2015.2429632) by W. 
% Bajwa, M. Duarte, M. and R. Calderbank is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% W.U. Bajwa, M.F. Duarte, and R. Calderbank, "Conditioning of random block subdictionaries with applications to block-sparse recovery and regression," IEEE Trans. 
% Information Theory, 2015, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and M.F. Duarte. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

function axisfortex (t,x,y)

set(gca,'FontSize',16);
set(gcf,'DefaultTextFontSize',16);
title (t); xlabel(x); ylabel(y);

