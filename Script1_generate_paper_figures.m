% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Conditioning of Random Block Subdictionaries with Applications to Block-Sparse Recovery and Regression," (DOI:10.1109/TIT.2015.2429632) by W. 
% Bajwa, M. Duarte, M. and R. Calderbank is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% W.U. Bajwa, M.F. Duarte, and R. Calderbank, "Conditioning of random block subdictionaries with applications to block-sparse recovery and regression," IEEE Trans. 
% Information Theory, 2015, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and M.F. Duarte. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

close all
clear all
addpath Data

% Parameters
N = 5000; % Signal length
G = 10; % Group size
K = 20; % Number of groups
Ms = round((sqrt(2*log(N/G-K))+sqrt(G))^2)+K*G; % Rao, Recht, Nowak
NMM = 10;
MMM = 11;
numiter = 2000;
% Set number of measurements
M = round(Ms*(0.75+MMM/4));

markv = {'none','+','*','x','o','s','d','^','v'}
colorv = [0   0    0;
1    0    0;
0    0.75 0.25;
0    0    1;
0.75 0    0.25;
1    0.5  0.5;
1    0    1;
0.5  1    0;
1    1    0];

load test_fixed_coherence_output
% Fig 4
figure(1)
for ii=1:size(SupRecmat,1),
    plot(Kvec,mean(SupRecmat(ii,:,:),3),'Color',colorv(mod(ii-1,size(colorv,1))+1,:),'Marker',markv{ii},'LineWidth',2),hold on
end
axisfortex('','Number of nonzero blocks, k','Probability of exact recovery')
legend('\tau=1','\tau=2','\tau=3','\tau=4')
axis tight

load test_minmax_coherence_recovery_output
% Fig 7
figure(2)
for ii=1:9,
    plot(Kvec,squeeze(mean(SupRecmat(ii,1,:,:),4)),'Color',colorv(mod(ii-1,size(colorv,1))+1,:),'Marker',markv{ii},'LineWidth',2), hold on
end
for ii=1:9,
    plot(Kvec,squeeze(mean(SupRecmat(ii,2,:,:),4)),'Color',colorv(mod(ii-1,size(colorv,1))+1,:),'Marker',markv{ii},'LineStyle','--','LineWidth',2)
end
axisfortex('','Number of nonzero blocks, k','Probability of exact recovery')
legend('\tau=1','\tau=2','\tau=3','\tau=4','\tau=5','\tau=6','\tau=7','\tau=8','\tau=9')
axis tight

load test_minmax_coherence_regression_output
figure(3)
for ii=1:5,
    plot(Kvec,squeeze(mean(SDRmat(end-8+ii,1,1:end,:),4)),'Color',colorv(mod(ii-1,size(colorv,1))+1,:),'Marker',markv{ii},'LineWidth',2),hold on
end
for ii=1:5,
    plot(Kvec,squeeze(mean(SDRmat(end-8+ii,2,1:end,:),4)),'Color',colorv(mod(ii-1,size(colorv,1))+1,:),'Marker',markv{ii},'LineStyle','--','LineWidth',2)
end
axisfortex('','Number of nonzero blocks, k','Regression error (ave. of 1000 trials)')
legend('\tau=3','\tau=4','\tau=5','\tau=6','\tau=7', 'Location', 'Best')
axis tight

print -depsc2 -f1 recon_samemu_markers.eps
print -depsc2 -f2 recon_extrememu_markers.eps
print -depsc2 -f3 regr_extrememu_markers.eps