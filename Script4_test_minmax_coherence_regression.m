% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Conditioning of Random Block Subdictionaries with Applications to Block-Sparse Recovery and Regression," (DOI:10.1109/TIT.2015.2429632) by W. 
% Bajwa, M. Duarte, M. and R. Calderbank is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% W.U. Bajwa, M.F. Duarte, and R. Calderbank, "Conditioning of random block subdictionaries with applications to block-sparse recovery and regression," IEEE Trans. 
% Information Theory, 2015, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and M.F. Duarte. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

addpath Data SpaRSA

% Parameters
N = 5000; % Signal length
G = 10; % Group size
K = 20; % Number of groups
groups = ceil((1:N)'/G); % Index group IDs
Ms = round((sqrt(2*log(N/G-K))+sqrt(G))^2)+K*G; % Number of measurements prescribed by [Rao, Recht, Nowak]
% Set number of spectral norm multipliers
NumTau = 10;
% Set number of random block sparse signal trials.
numiter = 1000;
% Set number of measurements
M = round(3.5*Ms);
% Set values of sparsity K to attempt
Kvec = 5:3:50;

SDRmat = zeros(NumTau,2,length(Kvec),numiter); % Matrix for signal recovery results

% Load matrix ensemble
load random_matrices_output

% For multiple values of spectral norm multiplier
for tau = 3:NumTau,
    [~,sortidx] = sort(Acohall(tau,:),'ascend');
    % For extreme (min/max) values of coherence
    for cnum = 1:2,
        % Select matrix with extreme coherence
        switch cnum
            case 1,
                testidx = sortidx(1);
            case 2,
                testidx = sortidx(end);
        end
        % Build seelcted random matrix
        rng(testidx);
        A = randn(M,N);
        % Normalize columns
        ANorm = sqrt(diag(A'*A));
        An = A./(ones(M,1)*ANorm');
        % Calculate SVD to adjust spectral norm
        [U,S,V] = svd(An);
        % Change spectral norm, rest of singular values
        bfact = sqrt((N-(tau*S(1))^2)/(N-S(1)^2));
        % Adjust singular value matrix
        S2 = [diag([S(1)*tau; bfact*diag(S(2:M,2:M))]) zeros(M,N-M)];
        % Build matrix from SVD
        Am = U*S2*V';
        % Normalize columns again
        AmNorm = sqrt(diag(Am'*Am));
        Amn = Am./(ones(M,1)*AmNorm');
    
        % Test matrix using group-sparse BP over numiter random signals per value of K
        for iter = 1:numiter,
            % For each value of K
            for Kidx = 1:length(Kvec),
                % Build random block sparse signal, normalize
                K = Kvec(Kidx);
                x = zeros(G,N/G);
                idx = randperm(N/G);
                x(:,idx(1:K)) = randn(G,K);
                x = x(:)/norm(x(:));
                % Signal and noise magnitude
                x = 6*x;
                sigma = sqrt(0.05); % Noise variance
                % Compute measurements
                y = Amn*x+sigma*randn(M,1);
                % Set up solver for Lasso
                hR = @(x) Amn*x;
                hRt = @(x) Amn'*x;
                
                % Lasso regularization parameter
                lambda = sqrt(log(N))*sigma*sqrt(G);
                
                psi = @(x,tau) group_vector_soft(x,tau,groups);
                phi = @(x) group_l2norm(x,groups);
                
                [x_SpaRSA,x_debias_SpaRSA,obj_SpaRSA,...
                    times_SpaRSA,debias_start_SpaRSA,mse]= ...
                    SpaRSA(y,hR,lambda,...
                    'Verbose',0,...
                    'Psi',psi,...
                    'Phi',phi,...
                    'Monotone',0,...
                    'Debias',1,...
                    'AT',hRt,...
                    'Initialization',1,...
                    'StopCriterion',1,...
                    'ToleranceA',1e-6);
                t_SpaRSA = times_SpaRSA(end);
                
                % Calculate and store group Lasso regression error squared norm
                if isempty(x_debias_SpaRSA),
                    sdr = norm(Amn*x)^2;
                else
                    sdr = norm(Amn*x-Amn*x_debias_SpaRSA)^2;
                end
                SDRmat(tau,cnum,Kidx,iter) = sdr;
            end
        end
        save test_minmax_coherence_regression_output SDRmat Kvec
    end
end