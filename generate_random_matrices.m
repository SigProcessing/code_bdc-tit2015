% Please refer to the 'README.txt' file for detailed instructions
% 
% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Conditioning of Random Block Subdictionaries with Applications to Block-Sparse Recovery and Regression," (DOI:10.1109/TIT.2015.2429632) by W. 
% Bajwa, M. Duarte, M. and R. Calderbank is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% W.U. Bajwa, M.F. Duarte, and R. Calderbank, "Conditioning of random block subdictionaries with applications to block-sparse recovery and regression," IEEE Trans. 
% Information Theory, 2015, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and M.F. Duarte. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

function generate_random_matrices
% Parameters
N = 5000; % Signal length
G = 10; % Group size
K = 20; % Number of groups
Ms = round((sqrt(2*log(N/G-K))+sqrt(G))^2)+K*G; % Number of measurements prescribed by [Rao, Recht, Nowak]
NumTau = 10; % Number of values of spectral norm multipliers
numiter = 2000; % Number of random seeds/matrices
% Store matrix norms, coherences, and SNRs
Anormhall = zeros(NumTau,numiter);
Acohall = Anormhall;
Abcohall = Anormhall;
Aicohall = Anormhall;
% Set number of measurements
M = round(3.5*Ms);
% For values of the random seed between 1 and numiter
for iter = 1:numiter,
    tic
    % Set random seed and build random matrix
    rng(iter)
    A = randn(M,N);
    % Normalize columns
    ANorm = sqrt(diag(A'*A));
    An = A./(ones(M,1)*ANorm');
    % Calculate SVD to adjust spectral nrom
    [U,S,V] = svd(An);
    % For each value of the spectral norm multiplier multiplier
    for tau = 1:NumTau,
        % Change spectral norm, rest of singular values
        bfact = sqrt((N-(tau*S(1))^2)/(N-S(1)^2));
        % Adjust singular value matrix
        S2 = [diag([S(1)*tau; bfact*diag(S(2:M,2:M))]) zeros(M,N-M)];
        % Build matrix from SVD decomposition
        Am = U*S2*V';
        % Normalize columns again
        AmNorm = sqrt(diag(Am'*Am));
        Amn = Am./(ones(M,1)*AmNorm');
        % Record spectral norm of this matrix
        Anormhall(tau,iter) = norm(Amn);
        % Compute coherence, intra-block, and block coherence
        mub = 0;
        mui = 0;
        muib = 0;
        % For each block in the matrix
        for i=1:N/G,
            Amni = Amn(:,(i-1)*G+(1:G));
            % Update coherence, intra-block coherence after considering this block
            mui = max(mui,max(max(abs(Amni'*Amni-eye(G)))));
            muib = max(mui,norm(Amni'*Amni-eye(G)));
            % For every further block in the matrix
            for j=(i+1):N/G,
                Amnj = Amn(:,(j-1)*G+(1:G));
                % Update block coherence
                mub = max(mub,norm(Amnj'*Amni));
            end
        end
        % Record coherence, intra-block, and block coherence
        Abcohall(tau,iter) = mub;
        Aicohall(tau,iter) = muib;
        Acohall(tau,iter) = mui;
    end
    toc
    if mod(iter,100) == 0,
        save random_matrices_output N G K M Anormhall Acohall Abcohall
    end
end